<?php

namespace App\Controller;

use DateInterval;
use DatePeriod;
use DateTime;
use IntlDateFormatter;
use stdClass;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use function count;

class CalendarController extends AbstractController
{
    /**
     * @Route("/", name="calendar")
     */
    public function index()
    {
        $dateTimeToday = new DateTime('today');
        $fmt = new IntlDateFormatter('fr_FR', IntlDateFormatter::FULL, IntlDateFormatter::NONE, 'Europe/Paris');
        $today = $fmt->format($dateTimeToday->getTimestamp());

        $months = [];

        $firstMonth = new DateTime('first day of previous month');
        $lastMonth = new DateTime('last day of +10 months');
        $intervalMonth = new DateInterval('P1M');
        $intervalDay = new DateInterval('P1D');

        $titleFormater = new IntlDateFormatter('fr_FR', IntlDateFormatter::FULL, IntlDateFormatter::NONE, 'Europe/Paris', IntlDateFormatter::GREGORIAN, 'MMMM yyyy');

        $periodMonth = new DatePeriod($firstMonth, $intervalMonth, $lastMonth);
        foreach ($periodMonth as $dateTimeMonth) {
            $month = new stdClass();
            $month->title = $titleFormater->format($dateTimeMonth->getTimestamp());
            $month->heads = ['L', 'M', 'M', 'J', 'V', 'S', 'D', null];
            $month->weeks = [];

            $firstDay = clone $dateTimeMonth;
            $lastDay = clone $dateTimeMonth;
            $lastDay->modify('last day of this month');
            $endDay = clone $dateTimeMonth;
            $endDay->modify('first day of next month');

            $periodDay = new DatePeriod($firstDay, $intervalDay, $endDay);
            foreach ($periodDay as $dateTimeDay) {
                if (!isset($week)) {
                    $week = new stdClass();
                    $week->num = $dateTimeDay->format('W');
                    $week->days = [];
                    for ($i = 1; $i <= 7; ++$i) {
                        if ($i === (int) $dateTimeDay->format('N')) {
                            break;
                        }

                        $week->days[] = null;
                    }
                }

                $day = new stdClass();
                $day->num = $dateTimeDay->format('d');
                $day->date = $dateTimeDay->format('Y-m-d');
                $week->days[] = $day;

                if ($dateTimeDay->format('d') === $lastDay->format('d')) {
                    while (count($week->days) < 7) {
                        $week->days[] = null;
                    }
                }

                if (7 === count($week->days)) {
                    $month->weeks[] = $week;
                    unset($week);
                }
            }

            $months[] = $month;
        }

        return $this->render('calendar/index.html.twig', [
            'today' => $today,
            'months' => $months,
        ]);
    }
}
