<?php

namespace App\Controller;

use DateInterval;
use DateTime;
use Exception;
use IntlDateFormatter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DateController extends AbstractController
{
    /**
     * @Route("/{date}", name="date", requirements={"date"="\d{4}-[0-1]\d-[0-3]\d"})
     * @param $date
     * @return Response
     */
    public function index($date)
    {
        try {
            $dateTimeEnd = new DateTime($date);
        } catch (Exception $e) {
            throw $this->createNotFoundException('Date invalide.');
        }

        $dateIntervalTotal = new DateInterval('P39W');

        $dateTimeBegin = clone $dateTimeEnd;
        $dateTimeBegin->sub($dateIntervalTotal);

        $dateTimeBegin = clone $dateTimeEnd;
        $dateTimeBegin->sub($dateIntervalTotal);

        $dateTimeToday = new DateTime('today');

        $dateIntervalRemain = $dateTimeEnd->diff($dateTimeToday);
        $dateIntervalPast = $dateTimeBegin->diff($dateTimeToday);

        $dayTotal = (int) ($dateIntervalTotal->format('%r%d'));
        $dayPast = (int) ($dateIntervalPast->format('%r%a'));
        $dayRemain = (int) ($dateIntervalRemain->format('%r%a'));

        $signPast = $dayPast < 0 ? '-' : '';
        $signRemain = $dayRemain < 0 ? '-' : '';

        $dayPast = abs($dayPast);
        $dayRemain = abs($dayRemain);

        $percentPast = (int) (100 * $dayPast / $dayTotal);

        $weekPast = abs((int) ($dayPast / 7));
        $weekRemain = abs((int) ($dayRemain / 7));
        $weekDayPast = abs($dayPast % 7);
        $weekDayRemain = abs($dayRemain % 7);

        $monthPast = (int) ($dateIntervalPast->format('%m'));
        $monthRemain = (int) ($dateIntervalRemain->format('%m'));
        $monthDayPast = (int) ($dateIntervalPast->format('%d'));
        $monthDayRemain = (int) ($dateIntervalRemain->format('%d'));
        $monthYearPast = (int) ($dateIntervalPast->format('%y'));
        $monthYearRemain = (int) ($dateIntervalRemain->format('%y'));

        $fmt = new IntlDateFormatter('fr_FR', IntlDateFormatter::FULL, IntlDateFormatter::NONE, 'Europe/Paris');
        $today = $fmt->format($dateTimeToday->getTimestamp());
        $end = $fmt->format($dateTimeEnd->getTimestamp());

        return $this->render('date/index.html.twig', [
            'today' => $today,
            'end' => $end,
            'dayPast' => $dayPast,
            'dayRemain' => $dayRemain,
            'dayTotal' => $dayTotal,
            'signPast' => $signPast,
            'signRemain' => $signRemain,
            'percentPast' => $percentPast,
            'weekPast' => $weekPast,
            'weekRemain' => $weekRemain,
            'weekDayPast' => $weekDayPast,
            'weekDayRemain' => $weekDayRemain,
            'monthPast' => $monthPast,
            'monthRemain' => $monthRemain,
            'monthDayPast' => $monthDayPast,
            'monthDayRemain' => $monthDayRemain,
            'monthYearPast' => $monthYearPast,
            'monthYearRemain' => $monthYearRemain,
        ]);
    }
}
